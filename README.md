# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

Candidate Number = 10058

## Project description

This project is a train dispatch system made for the portfolio project given in Programmering 1 (IDATT1003) in the fall of 2023. The assignment was to design and code a simplified train dispatch system, with an entity, entity register, and text-based UI. JUnit test classes and JavaDoc were also required. 

## Project structure

Source files are stored in the under the _traindispatchsystem/src/main/java/edu.ntnu.stud_ folder. Among the source files are an entity class, an entity register class, an enum, a validation class, a user interface, and an app class. 

In the _traindispatchsystem/src/test/java/edu.ntnu.stud_ folder are JUnit 5 tests for TrainDeparture, TrainDepartureRegister, and ValidityManager.

- Entity -> TrainDeparture
- Entity Register -> TrainDepartureRegister
- Enum -> TrainLines
- Validation -> ValidityManager
- User interface -> UserInterface
- App -> TrainDispatchApp

## Link to repository

https://gitlab.stud.idi.ntnu.no/cacorapi/traindispatchsystem.git

## How to run the project

[//]: # (TODO: Describe how to run your project here. What is the main class? What is the main method?
What is the input and output of the program? What is the expected behaviour of the program?)
This program uses a text-based interface, which the user interacts with using the IntelliJ Idea integrated console. To run the project, simply run TrainDispatchApp, which contains the main method, and follow the instructions given. The user types numbers and words, depending on the function they choose to use in the program. The output will always be in String format.

## How to run the tests

[//]: # (TODO: Describe how to run the tests here.)
JUnit 5 Jupiter unit testing is used in this program. To run any tests, choose a test file in _traindispatchsystem/src/test/java/edu.ntnu.stud_ and click the go button on the left of the class declaration. All of the tests will automatically execute, and their results will be shown in the console.

## References

N/A
