package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.LocalTime;

/** Test class for TrainDepartureRegister.
 * GitHub Copilot used to generate structure of similar tests
 *
 * @since 2.1
 * @version 3.3
 * @author 10058
 * @see TrainDepartureRegister
 * */
public class TrainDepartureRegisterTest {
  
  private ValidityManager validityManager;
  
  @BeforeEach
  void setUp() {
    validityManager = new ValidityManager();
    
  }
  
  @Nested
  @DisplayName("Initialization")
  class Initialization {
    
    private TrainDepartureRegister trainDepartureRegister;
    
    @Test
    @DisplayName("Test valid train departure initialization")
    void testValidTrainDepartureInitialization() {
      trainDepartureRegister = new TrainDepartureRegister(LocalTime.of(0, 0),
          validityManager);
      assertEquals(LocalTime.of(0, 0), trainDepartureRegister.getTime());
      assertNotNull(validityManager);
    }
    
    @Test
    @DisplayName("Test train departure register initialization with invalid time parameter")
    void testInvalidTimeInitialization() {
      try {
        trainDepartureRegister = new TrainDepartureRegister(LocalTime.of(-1, 0),
            validityManager);
        fail("TrainDepartureRegister initialization with invalid time parameter not implemented correctly, "
            + "should throw date time exception");
      } catch (DateTimeException e) {
        assertEquals("Invalid value for HourOfDay (valid values 0 - 23): -1", e.getMessage());
      }
    }
  }
  
  @Nested
  @DisplayName("Getter methods")
  class GetterMethods {
    
    private TrainDepartureRegister trainDepartureRegister;
    
    @BeforeEach
    void setUp() {
      trainDepartureRegister = new TrainDepartureRegister(LocalTime.of(0, 0),
          validityManager);
    }
    
    @Test
    @DisplayName("Test getTrainDepartures while empty")
    void testGetTrainDeparturesWhileEmpty() {
      assertTrue(trainDepartureRegister.getTrainDepartures().isEmpty());
    }
    
    @Test
    @DisplayName("Test getTrainDepartures while not empty")
    void testGetTrainDeparturesWhileNotEmpty() {
      trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 30), LocalTime.of(0, 0));
      
      assertEquals(1, trainDepartureRegister.getTrainDepartures().size());
    }
    
    @Test
    @DisplayName("Test getTime")
    void testGetTime() {
      assertEquals(LocalTime.of(0, 0), trainDepartureRegister.getTime());
    }
    
    @Test
    @DisplayName("Test getTrainDepartureFromNumber with valid train number parameter")
    void testGetTrainDepartureFromNumberWithValidTrainNumber() {
      trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 30), LocalTime.of(0, 0));
      
      assertEquals(113, trainDepartureRegister.getTrainDepartureFromNumber(113).getTrainNumber());
    }
    
    @Test
    @DisplayName("Test getTrainDepartureFromNumber with invalid train number parameter")
    void testGetTrainDepartureFromNumberWithInvalidTrainNumber() {
      try {
        trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
            0, LocalTime.of(10, 30), LocalTime.of(0, 0));
        trainDepartureRegister.getTrainDepartureFromNumber(114);
        
        fail("getTrainDepartureFromNumber with invalid train number parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Train with number 114 does not exist", e.getMessage());
      }
    }
  }
  
  @Nested
  @DisplayName("Mutator methods")
  class MutatorMethods {
    
    private TrainDepartureRegister trainDepartureRegister;
    @BeforeEach
    void setUp() {
      trainDepartureRegister = new TrainDepartureRegister(LocalTime.of(0, 0), validityManager);
    }
    
    @Test
    @DisplayName("Test setTime with valid time  and initial context parameters")
    void testSetInitialTimeWithValidTimeAndInitialContext() {
      trainDepartureRegister.setTime(LocalTime.of(10, 30), "initial");
      
      assertEquals(LocalTime.of(10, 30), trainDepartureRegister.getTime());
    }
    
    @Test
    @DisplayName("Test setTime with invalid time and initial context parameters")
    void testSetInitialTimeWithInvalidTimeAndInitialContext() {
      try {
        trainDepartureRegister.setTime(LocalTime.of(-1, 0), "initial");
        
        fail("setTime with invalid time not implemented correctly, "
            + "should throw date time exception");
      } catch (DateTimeException e) {
        assertEquals("Invalid value for HourOfDay (valid values 0 - 23): -1", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test setTime with valid time and later context parameters")
    void testSetTimeWithValidTimeAndLaterContext() {
      trainDepartureRegister.setTime(LocalTime.of(10, 15), "initial");
      trainDepartureRegister.setTime(LocalTime.of(10, 30), "later");
      assertEquals(LocalTime.of(10, 30), trainDepartureRegister.getTime());
    }
    
    @Test
    @DisplayName("Test setTime with invalid (negative) time and later context parameter")
    void testSetLaterTimeWithInvalidNegativeTime() {
      try {
        trainDepartureRegister.setTime(LocalTime.of(10, 15), "initial");
        trainDepartureRegister.setTime(LocalTime.of(-1, 0), "later");
        
        fail("setTime with invalid time not implemented correctly, "
            + "should throw date time exception");
      } catch (DateTimeException e) {
        assertEquals("Invalid value for HourOfDay (valid values 0 - 23): -1", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test setTime with invalid (early) time and later context parameter")
    void testSetLaterTimeWithInvalidEarlyTimeAndLaterContext() {
      try {
        trainDepartureRegister.setTime(LocalTime.of(10, 15), "initial");
        trainDepartureRegister.setTime(LocalTime.of(10, 0), "later");
        
        fail("setTime with invalid time not implemented correctly, "
            + "should throw date time exception");
      } catch (DateTimeException e) {
        assertEquals("Departure time 10:00 is invalid, it must be after the previous departure time 10:15", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test addTrainDeparture with valid parameters")
    void testAddTrainDepartureWithValidParameters() {
      trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 30), LocalTime.of(0, 0));
      
      assertEquals(113, trainDepartureRegister.getTrainDepartureFromNumber(113).getTrainNumber());
    }
    
    @Test
    @DisplayName("Test addTrainDeparture with invalid train number parameter")
    void testAddTrainDepartureWithInvalidTrainNumberParameter() {
      try {
        trainDepartureRegister.addTrainDeparture(-113, "Barcelona", TrainLines.L1,
            0, LocalTime.of(10, 30), LocalTime.of(0, 0));
        
        fail("addTrainDeparture with invalid train number parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
          assertEquals("Train number cannot be less than or equal to 0", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test addTrainDeparture with invalid destination parameter")
    void testAddTrainDepartureWithInvalidDestinationParameter() {
      try {
        trainDepartureRegister.addTrainDeparture(113, "", TrainLines.L1,
            0, LocalTime.of(10, 30), LocalTime.of(0, 0));
        
        fail("addTrainDeparture with invalid destination parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
          assertEquals("Destination cannot be null or blank", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test addTrainDeparture with invalid parameter")
    void testAddTrainDepartureWithInvalidLineParameter() {
      try {
        trainDepartureRegister.addTrainDeparture(113, "Barcelona", null,
            0, LocalTime.of(10, 30), LocalTime.of(0, 0));
        
        fail("addTrainDeparture with invalid line parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Line cannot be null", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test addTrainDeparture with invalid track parameter")
    void testAddTrainDepartureWithInvalidTrackParameter() {
      try {
        trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
            -5, LocalTime.of(10, 30), LocalTime.of(0, 0));
        
        fail("addTrainDeparture with invalid track parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Invalid track number: -5", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test addTrainDeparture with invalid departure time parameter")
    void testAddTrainDepartureWithInvalidDepartureTimeParameter() {
      try {
        trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
            0, LocalTime.of(30, 30), LocalTime.of(0, 0));
        
        fail("addTrainDeparture with invalid departure time parameter not implemented correctly, "
            + "should throw date time exception");
      } catch (DateTimeException e) {
        assertEquals("Invalid value for HourOfDay (valid values 0 - 23): 30", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test addTrainDeparture with invalid delay parameter")
    void testAddTrainDepartureWithInvalidDelayParameter() {
      try {
        trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
            0, LocalTime.of(-10, 30), LocalTime.of(0, 0));
        
        fail("addTrainDeparture with invalid departure time parameter not implemented correctly, "
            + "should throw date time exception");
      } catch (DateTimeException e) {
        assertEquals("Invalid value for HourOfDay (valid values 0 - 23): -10", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test sortTrainDeparturesByDepartureTime")
    void testSortTrainDeparturesByDepartureTime() {
      trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 30), LocalTime.of(0, 0));
      trainDepartureRegister.addTrainDeparture(114, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 0), LocalTime.of(0, 0));
      trainDepartureRegister.addTrainDeparture(115, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 15), LocalTime.of(0, 0));
      
      trainDepartureRegister.sortTrainDeparturesByDepartureTime();
      
      assertEquals(114, trainDepartureRegister.getTrainDepartures().entrySet().iterator().next()
          .getValue().getTrainNumber());
    }
    
    @Test
    @DisplayName("Test sortTrainDeparturesByDepartureTime with empty train departure register")
    void testSortTrainDeparturesByDepartureTimeWithEmptyTrainDepartureRegister() {
      trainDepartureRegister.sortTrainDeparturesByDepartureTime();
      
      assertTrue(trainDepartureRegister.getTrainDepartures().isEmpty());
    }
    
    @Test
    @DisplayName("Test removeDepartedTrains")
    void testRemoveDepartedTrains() {
      trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 30), LocalTime.of(0, 0));
      trainDepartureRegister.addTrainDeparture(114, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 0), LocalTime.of(0, 0));
      trainDepartureRegister.addTrainDeparture(115, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 15), LocalTime.of(0, 0));
      
      trainDepartureRegister.setTime(LocalTime.of(10, 10), "initial");
      
      assertEquals(2, trainDepartureRegister.getTrainDepartures().size());
    }
    
    @Test
    @DisplayName("Test updateDelay with valid parameters")
    void testUpdateDelayWithValidParameters() {
      trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 30), LocalTime.of(0, 0));
      
      trainDepartureRegister.updateDelay(LocalTime.of(0, 30), 113);
      
      assertEquals(LocalTime.of(11, 0), trainDepartureRegister.getTrainDepartureFromNumber(113)
          .getDelayedDepartureTime());
    }
    
    @Test
    @DisplayName("Test updateDelay with invalid delay parameter")
    void testUpdateDelayWithInvalidDelayParameter() {
      try{
        trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
            0, LocalTime.of(10, 30), LocalTime.of(0, 0));
        trainDepartureRegister.updateDelay(LocalTime.of(0, 90), 113);
        fail("updateDelay with invalid delay parameter not implemented correctly, "
            + "should throw date time exception");
      } catch (DateTimeException e) {
        assertEquals("Invalid value for MinuteOfHour (valid values 0 - 59): 90", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test updateDelay with too large delay parameter")
    void testUpdateDelayWithTooLargeDelayParameter() {
      try{
        trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
            0, LocalTime.of(10, 30), LocalTime.of(0, 0));
        trainDepartureRegister.updateDelay(LocalTime.of(23, 59), 113);
        
        fail("updateDelay with invalid delay parameter not implemented correctly, "
            + "should illegal argument exception");
      } catch (DateTimeException e) {
        assertEquals("The delay 23:59 cannot make departure time exceed 23:59", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test updateDelay with invalid train number parameter")
    void testUpdateDelayWithInvalidTrainNumberParameter() {
      try {
      trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 30), LocalTime.of(0, 0));
      trainDepartureRegister.updateDelay(LocalTime.of(3, 59), -113);
      
      fail("updateDelay with invalid train number parameter not implemented correctly, "
          + "should throw illegal argument exception");
    } catch (IllegalArgumentException e) {
      assertEquals("Train number cannot be less than or equal to 0", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test updateTrack with valid parameters")
    void testUpdateTrackWithValidParameters() {
      trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 30), LocalTime.of(0, 0));
      
      trainDepartureRegister.updateTrack(1, 113);
      
      assertEquals(1, trainDepartureRegister.getTrainDepartureFromNumber(113).getTrack());
    }
    
    @Test
    @DisplayName("Test updateTrack with invalid train number parameter")
    void testUpdateTrackWithInvalidTrainNumberParameter() {
      try {
        trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
            0, LocalTime.of(10, 30), LocalTime.of(0, 0));
        trainDepartureRegister.updateTrack(5, -113);
        
        fail("updateTrack with invalid train number parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Train number cannot be less than or equal to 0", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test updateTrack with invalid track parameter")
    void testUpdateTrackWithInvalidTrackParameter() {
      try {
        trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
            0, LocalTime.of(10, 30), LocalTime.of(0, 0));
        trainDepartureRegister.updateTrack(-5, 113);
        
        fail("updateTrack with invalid track parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Invalid track number: -5", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test removeTrack with valid parameters")
    void testRemoveTrackWithValidParameters() {
      trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 30), LocalTime.of(0, 0));
      
      trainDepartureRegister.removeTrack( 113);
      
      assertEquals(-1, trainDepartureRegister.getTrainDepartureFromNumber(113).getTrack());
    }
    @Test
    @DisplayName("Test remove Track with invalid train number parameter")
    void testRemoveTrackWithInvalidTrainNumberParameter() {
      try {
        trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
            0, LocalTime.of(10, 30), LocalTime.of(0, 0));
        trainDepartureRegister.removeTrack( -113);
        
        fail("removeTrack with invalid train number parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Train number cannot be less than or equal to 0", e.getMessage());
      }
    }
  }
  
  @Nested
  @DisplayName("toString methods")
  class toStringMethods {
    
    private TrainDepartureRegister trainDepartureRegister;
    
    @BeforeEach
    void setUp() {
      trainDepartureRegister = new TrainDepartureRegister(LocalTime.of(0, 0), validityManager);
    }
    
    @Test
    @DisplayName("Test getTrainsFromDestination with valid destination parameter")
    void testGetTrainsFromDestinationWithValidDestination() {
      trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
          1, LocalTime.of(10, 30), LocalTime.of(0, 0));
      trainDepartureRegister.addTrainDeparture(114, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 0), LocalTime.of(0, 0));
      trainDepartureRegister.addTrainDeparture(115, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 15), LocalTime.of(0, 0));
      
      String expected = String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
          "Train", "Destination", "Line", "Track", "Departure Time", "Delay", "Delayed Departure Time")
          + "\n" + String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
          "113", "Barcelona", "L1", "1", "10:30", "00:00", "")
          + "\n" + String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
          "114", "Barcelona", "L1", "", "10:00", "00:00", "")
          + "\n" + String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
          "115", "Barcelona", "L1", "", "10:15", "00:00", "");
      
      assertEquals(expected, trainDepartureRegister.getTrainDepartureFromDestination("Barcelona"));
    }
    
    @Test
    @DisplayName("Test getTrainsFromDestination with invalid destination parameter")
    void testGetTrainsFromDestinationWithInValidDestination() {
      try {
        trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
            1, LocalTime.of(10, 30), LocalTime.of(0, 0));
        trainDepartureRegister.addTrainDeparture(114, "Barcelona", TrainLines.L1,
            0, LocalTime.of(10, 0), LocalTime.of(0, 0));
        trainDepartureRegister.addTrainDeparture(115, "Barcelona", TrainLines.L1,
            0, LocalTime.of(10, 15), LocalTime.of(0, 0));
        
        trainDepartureRegister.getTrainDepartureFromDestination("");
        fail("getTrainsFromDestination with invalid destination parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Destination cannot be null or blank", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test toString with valid parameters")
    void testToString() {
      trainDepartureRegister.addTrainDeparture(113, "Barcelona", TrainLines.L1,
          1, LocalTime.of(10, 30), LocalTime.of(0, 0));
      trainDepartureRegister.addTrainDeparture(114, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 0), LocalTime.of(0, 0));
      trainDepartureRegister.addTrainDeparture(115, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 15), LocalTime.of(0, 0));
      
      String expected = String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
          "Train", "Destination", "Line",
          "Track", "Departure Time", "Delay", "Delayed Departure Time")
          + "\n-----------------------------------------------------------------------"
          + "-------------------------------------------------------------------"
          + "\n" + String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
          "114", "Barcelona", "L1", "", "10:00", "00:00", "")
          + "\n" + String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
          "115", "Barcelona", "L1", "", "10:15", "00:00", "")
          + "\n" + String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
          "113", "Barcelona", "L1", "1", "10:30", "00:00", "");
      
      assertEquals(expected, trainDepartureRegister.toString());
    }
  }
}