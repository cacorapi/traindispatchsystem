package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.LocalTime;

/**Test class for the TrainDeparture class.
 *GitHub Copilot used to generate structure of similar tests
 *
 * @since 2.0
 * @version 3.3
 * @author 10058
 * @see TrainDeparture
 */
public class TrainDepartureTest {
  
  private ValidityManager validityManager;
  
  @BeforeEach
  void setUp() {
    validityManager = new ValidityManager();
    
  }
  
  @Nested
  @DisplayName("Initialization")
  class Initialization {
    
    TrainDeparture trainDeparture;
    TrainDeparture trainDeparture2;
    
    @Test
    @DisplayName("Test valid train departure initialization without delay")
    void testValidTrainDepartureInitializationWithoutDelay() {
      trainDeparture = new TrainDeparture(113, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 30), LocalTime.of(0, 0), validityManager);
      
      assertEquals(113, trainDeparture.getTrainNumber());
      assertEquals("Barcelona", trainDeparture.getDestination());
      assertEquals(TrainLines.L1, trainDeparture.getLine());
      assertEquals(-1, trainDeparture.getTrack());
      assertEquals(LocalTime.of(10, 30), trainDeparture.getDepartureTime());
      assertEquals(LocalTime.of(0, 0), trainDeparture.getDelay());
      assertEquals(LocalTime.of(10, 30), trainDeparture.getDelayedDepartureTime());
      assertNotNull(validityManager);
    }
    
    @Test
    @DisplayName("Test valid train departure initialization with delay")
    void testValidTrainDepartureInitializationWithDelay() {
      trainDeparture = new TrainDeparture(113, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 30), LocalTime.of(1, 20), validityManager);
      
      assertEquals(113, trainDeparture.getTrainNumber());
      assertEquals("Barcelona", trainDeparture.getDestination());
      assertEquals(TrainLines.L1, trainDeparture.getLine());
      assertEquals(-1, trainDeparture.getTrack());
      assertEquals(LocalTime.of(10, 30), trainDeparture.getDepartureTime());
      assertEquals(LocalTime.of(1, 20), trainDeparture.getDelay());
      assertEquals(LocalTime.of(11, 50), trainDeparture.getDelayedDepartureTime());
      assertNotNull(validityManager);
    }
    
    @Test
    @DisplayName("Test train departure initialization with invalid train number parameter (number already in use)")
    void testInvalidTrainNumberInitializationBecauseTaken() {
      try {
        trainDeparture = new TrainDeparture(113, "Barcelona", TrainLines.L1,
            0, LocalTime.of(10, 30), LocalTime.of(0, 0), validityManager);
        trainDeparture2 = new TrainDeparture(113, "Barcelona", TrainLines.L1,
            0, LocalTime.of(11, 30), LocalTime.of(0, 0), validityManager);
        fail("TrainDeparture initialization with invalid train number parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Train with number 113 already exists", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test train departure initialization with invalid train number parameter (negative number)")
    void testInvalidTrainNumberInitializationBecauseNegative() {
      try {
        trainDeparture = new TrainDeparture(-113, "Barcelona", TrainLines.L1,
            0, LocalTime.of(10, 30), LocalTime.of(0, 0), validityManager);
        fail("TrainDeparture initialization with invalid train number parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Train number cannot be less than or equal to 0", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test train departure initialization with invalid destination parameter")
    void testInvalidDestinationInitialization() {
      try {
        trainDeparture = new TrainDeparture(113, "", TrainLines.L1,
            0, LocalTime.of(10, 30), LocalTime.of(0, 0), validityManager);
        fail("TrainDeparture initialization with invalid destination parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Destination cannot be null or blank", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test train departure initialization with invalid line parameter")
    void testInvalidLineInitialization() {
      try {
        trainDeparture = new TrainDeparture(113, "Barcelona", null,
            0, LocalTime.of(10, 30), LocalTime.of(0, 0), validityManager);
        fail("TrainDeparture initialization with invalid line parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Line cannot be null", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test train departure initialization with invalid track parameter")
    void testInvalidTrackInitialization() {
      try {
        trainDeparture = new TrainDeparture(113, "Barcelona", TrainLines.L1,
            -5, LocalTime.of(10, 30), LocalTime.of(0, 0), validityManager);
        fail("TrainDeparture initialization with invalid line parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Invalid track number: -5", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test train departure initialization with invalid departure time parameter")
    void testInvalidDepartureTimeInitialization() {
      try {
        trainDeparture = new TrainDeparture(113, "Barcelona", TrainLines.L1,
            0, LocalTime.of(30, 30), LocalTime.of(0, 0), validityManager);
        fail("TrainDeparture initialization with invalid departure time parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (java.time.DateTimeException e) {
        assertEquals("Invalid value for HourOfDay (valid values 0 - 23): 30", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test train departure initialization with invalid delay parameter")
    void testInvalidDelayInitialization() {
      try {
        trainDeparture = new TrainDeparture(113, "Barcelona", TrainLines.L1,
            0, LocalTime.of(10, 30), LocalTime.of(-10, 0), validityManager);
        fail("TrainDeparture initialization with invalid delay parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (java.time.DateTimeException e) {
        assertEquals("Invalid value for HourOfDay (valid values 0 - 23): -10", e.getMessage());
      }
    }
  }
  
  @Nested
  @DisplayName("Getter methods")
  class GetterMethods {
    
    private TrainDeparture trainDeparture;
    
    @BeforeEach
    void setUp() {
      trainDeparture = new TrainDeparture(113, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 30), LocalTime.of(0, 0), validityManager);
    }
    
    @Test
    @DisplayName("Test getTrainNumber")
    void testGetTrainNumber() {
      assertEquals(113, trainDeparture.getTrainNumber());
    }
    
    @Test
    @DisplayName("Test getDestination")
    void testGetDestination() {
      assertEquals("Barcelona", trainDeparture.getDestination());
    }
    
    @Test
    @DisplayName("Test getLine")
    void testGetLine() {
      assertEquals(TrainLines.L1, trainDeparture.getLine());
    }
    
    @Test
    @DisplayName("Test getDepartureTime")
    void testGetDepartureTime() {
      assertEquals(LocalTime.of(10, 30), trainDeparture.getDepartureTime());
    }
    
    @Test
    @DisplayName("Test getTrack")
    void testGetTrack() {
      assertEquals(-1, trainDeparture.getTrack());
    }
    
    @Test
    @DisplayName("Test getDelay")
    void testGetDelay() {
      assertEquals(LocalTime.of(0, 0), trainDeparture.getDelay());
    }
    
    @Test
    @DisplayName("Test getDelayedDepartureTime")
    void testGetDelayedDepartureTime() {
      assertEquals(LocalTime.of(10, 30), trainDeparture.getDelayedDepartureTime());
    }
    
  }
  
  @Nested
  @DisplayName("Mutator methods")
  class MutatorMethods {
    
    private TrainDeparture trainDeparture;
    
    @BeforeEach
    void setUp() {
      trainDeparture = new TrainDeparture(113, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 30), LocalTime.of(0, 0), validityManager);
    }
    
    @Test
    @DisplayName("Test setTrack when track parameter is unassigned")
    void testSetTrackWhenUnassigned() {
      trainDeparture.setTrack(0);
      assertEquals(-1, trainDeparture.getTrack());
    }
    
    @Test
    @DisplayName("Test setTrack when track parameter is assigned ")
    void testSetTrackWhenAssigned() {
      trainDeparture.setTrack(2);
      assertEquals(2, trainDeparture.getTrack());
    }
    
    @Test
    @DisplayName("Test setTrack with invalid track parameter")
    void testSetTrackWithInvalidTrack() {
      try {
        trainDeparture.setTrack(-2);
        fail("setTrack with invalid track parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Invalid track number: -2", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test removeTrack")
    void testRemoveTrack() {
      trainDeparture.setTrack(2);
      trainDeparture.removeTrack();
      
      assertNotEquals(2, trainDeparture.getTrack());
      assertEquals(-1, trainDeparture.getTrack());
    }
    
    @Test
    @DisplayName("Test setDelay with initial delay parameter")
    void testSetDelayWithFirstDelay() {
      trainDeparture.setDelay(LocalTime.of(0, 30), "later");
      
      assertNotEquals(LocalTime.of(0, 0), trainDeparture.getDelay());
      assertEquals(LocalTime.of(0, 30), trainDeparture.getDelay());
    }
    
    @Test
    @DisplayName("Test setDelay with later delay parameter")
    void testSetDelayWithSecondDelay() {
      trainDeparture.setDelay(LocalTime.of(0, 30), "later");
      trainDeparture.setDelay(LocalTime.of(0, 30), "later");
      
      assertNotEquals(LocalTime.of(0, 0), trainDeparture.getDelay());
      assertNotEquals(LocalTime.of(0, 30), trainDeparture.getDelay());
      assertEquals(LocalTime.of(1, 0), trainDeparture.getDelay());
    }
    
    @Test
    @DisplayName("Test setDelay with invalid delay parameter")
    void testSetDelayWithInvalidDelay() {
      try {
        trainDeparture.setDelay(LocalTime.of(-2, 30), "later");
        fail("setDelay with invalid delay parameter not implemented correctly, "
            + "should throw illegal argument exception");
      } catch (DateTimeException e) {
        assertEquals("Invalid value for HourOfDay (valid values 0 - 23): -2", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test setDelayDepartureTime through setDelay")
    void testDelayedDepartureTimeWithSetDelay() {
      trainDeparture.setDelay(LocalTime.of(0, 30), "later");
      
      assertNotEquals(LocalTime.of(10, 30), trainDeparture.getDelayedDepartureTime());
      assertEquals(LocalTime.of(11, 0), trainDeparture.getDelayedDepartureTime());
    }
    
    @Test
    @DisplayName("Test setDelayDepartureTime with zero delay parameter")
    void testSetDelayedDepartureTimeWithNoDelay() {
      assertEquals(LocalTime.of(10, 30), trainDeparture.getDelayedDepartureTime());
    }
  }
  
  @Nested
  @DisplayName("toString methods")
  class toStringMethods {
    private TrainDeparture trainDeparture;
    
    @BeforeEach
    void setUp() {
      trainDeparture = new TrainDeparture(113, "Barcelona", TrainLines.L1,
          0, LocalTime.of(10, 30), LocalTime.of(0, 0), validityManager);
    }
    
    @Test
    @DisplayName("Test toString without delay and with an assigned track parameter")
    void testToStringWithoutDelayAndWithAssignedTrack() {
      trainDeparture.setTrack(1);
      String expected = String.format("Train number: %d%nDestination: %s%nLine: %s%nTrack: %d%nDeparture"
          + " Time: %s%nDelay: %s", 113, "Barcelona", TrainLines.L1, 1, LocalTime
          .of(10, 30), LocalTime.of(0, 0));
      
      assertEquals(expected, trainDeparture.toString());
    }
    
    @Test
    @DisplayName("Test toString without delay and with an unassigned track parameter")
    void testToStringWithoutDelayAndWithUnassignedTrack() {
      String expected = String.format("Train number: %d%nDestination: %s%nLine: %s%nTrack: %s%nDeparture"
          + " Time: %s%nDelay: %s", 113, "Barcelona", TrainLines.L1, "", LocalTime
          .of(10, 30), LocalTime.of(0, 0));
      
      assertEquals(expected, trainDeparture.toString());
    }
    
    @Test
    @DisplayName("Test toString with delay and with an assigned track parameter")
    void testToStringWithDelayAndAssignedTrick() {
      trainDeparture.setDelay(LocalTime.of(0, 30), "later");
      trainDeparture.setTrack(1);
      String expected = String.format("Train number: %d%nDestination: %s%nLine: %s%nTrack: %d%nDeparture"
          + " Time: %s%nDelay: %s%nDelayed Departure Time: %s%n", 113, "Barcelona", TrainLines.L1, 1, LocalTime
          .of(10, 30), LocalTime.of(0, 30), LocalTime.of(11, 0));
      
      assertEquals(expected, trainDeparture.toString());
    }
    @Test
    @DisplayName("Test toString with delay and with an unassigned track parameter")
    void testToStringWithDelayAndUnassignedTrack() {
      trainDeparture.setDelay(LocalTime.of(0, 30), "later");
      String expected = String.format("Train number: %d%nDestination: %s%nLine: %s%nTrack: %s%nDeparture"
          + " Time: %s%nDelay: %s%nDelayed Departure Time: %s%n", 113, "Barcelona", TrainLines.L1, "", LocalTime
          .of(10, 30), LocalTime.of(0, 30), LocalTime.of(11, 0));
      
      assertEquals(expected, trainDeparture.toString());
    }
    
    @Test
    @DisplayName("Test toSingleLineString without delay and with an assigned track parameter")
    void testToSingleLineStringWithoutDelayAndAssignedTrack() {
      trainDeparture.setTrack(1);
      String expected = String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
          "113", "Barcelona", "L1", "1", "10:30", "00:00", "");
      assertEquals(expected, trainDeparture.toSingleLineString());
    }
    @Test
    @DisplayName("Test toSingleLineString without delay and with an unassigned track parameter")
    void testToSingleLineStringWithoutDelayAndUnassignedTrack() {
      String expected = String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
          "113", "Barcelona", "L1", "", "10:30", "00:00", "");
      assertEquals(expected, trainDeparture.toSingleLineString());
    }
    
    @Test
    @DisplayName("Test toSingleLineString with delay and with an assigned track parameter")
    void testToSingleLineStringWithDelayAndWithAssignedTrack() {
      trainDeparture.setDelay(LocalTime.of(0, 30), "later");
      trainDeparture.setTrack(1);
      String expected = String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s | ",
          "113", "Barcelona", "L1", "1", "10:30", "00:30", "11:00");
      String unexpected = String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
          "113", "Barcelona", "L1", "-1", "10:30", "00:00", "");
      
      assertNotEquals(unexpected, trainDeparture.toSingleLineString());
      assertEquals(expected, trainDeparture.toSingleLineString());
    }
    
    @Test
    @DisplayName("Test toSingleLineString with delay and with an unassigned track parameter")
    void testToSingleLineStringWithDelayAndWithUnassignedTrack() {
      trainDeparture.setDelay(LocalTime.of(0, 30), "later");
      String expected = String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s | ",
          "113", "Barcelona", "L1", "", "10:30", "00:30", "11:00");
      String unexpected = String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
          "113", "Barcelona", "L1", "-1", "10:30", "00:00", "");
      
      assertNotEquals(unexpected, trainDeparture.toSingleLineString());
      assertEquals(expected, trainDeparture.toSingleLineString());
    }
  }
}