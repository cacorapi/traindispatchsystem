package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.LocalTime;

/**
 * Test class for the ValidityManager class.
 *GitHub Copilot used to generate structure of similar tests
 *
 * @since 2.3
 * @version 3.3
 * @author 10058
 * @see ValidityManager
 */
public class ValidityManagerTest {
  
  @Nested
  @DisplayName("Initialization")
  class Initialization {
    
    @Test
    @DisplayName("Should initialize collections")
    void shouldInitializeCollections() {
      ValidityManager validityManager = new ValidityManager();
      assertNotNull(validityManager.getOccupiedTracks());
      assertEquals(0, validityManager.getOccupiedTracks().size());
      assertNotNull(validityManager.getTrainNumbers());
      assertEquals(0, validityManager.getTrainNumbers().size());
      assertNotNull(validityManager.getDestinations());
      assertEquals(0, validityManager.getDestinations().size());
    }
  }
  
  @Nested
  @DisplayName("Validation methods")
  class ValidationMethods {
    
    private ValidityManager validityManager;
    
    @BeforeEach
    void setUp() {
      validityManager = new ValidityManager();
    }
    
    @Test
    @DisplayName("Test validateTimeParameter with valid time parameter")
    void testValidateTimeParameterWithValidTime() {
      try {
        validityManager.validateTimeParameter(LocalTime.of(12, 0), LocalTime.of(11, 0));
      } catch (DateTimeException e) {
        fail("Should not throw date time exception");
      }
    }
    
    @Test
    @DisplayName("Test validateTimeParameter with invalid time parameter")
    void testValidateTimeParameterWithInvalidTime() {
      try {
        validityManager.validateTimeParameter(LocalTime.of(-24, 0), LocalTime.of(0, 0));
        fail("Should throw illegal argument exception");
      } catch (DateTimeException e) {
        assertEquals("Invalid value for HourOfDay (valid values 0 - 23): -24",
            e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test validateTimeParameter with invalid (early) time parameter")
    void testValidateTimeParameterWithInvalidEarlyTime() {
      try {
        validityManager.validateTimeParameter(LocalTime.of(10, 0), LocalTime.of(12, 0));
        fail("Should throw illegal argument exception");
      } catch (DateTimeException e) {
        assertEquals("Departure time 10:00 is invalid, it must be after the previous departure time 12:00",
            e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test validateDestinationParameter with valid destination parameter")
    void testValidateDestinationParameterWithValidDestination() {
      try {
        validityManager.validateDestinationParameter("Oslo", "initial");
      } catch (IllegalArgumentException e) {
        fail("Should not throw illegal argument exception");
      }
    }
    
    @Test
    @DisplayName("Test validateDestinationParameter with invalid destination parameter")
    void testValidateDestinationParameterWithInvalidDestination() {
      try {
        validityManager.validateDestinationParameter("", "initial");
        fail("Should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Destination cannot be null or blank", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test validateLineParameter with valid line parameter")
    void testValidateLineParameterWithValidLine() {
      try {
        validityManager.validateLineParameter(TrainLines.L1);
      } catch (IllegalArgumentException e) {
        fail("Should not throw illegal argument exception");
      }
    }
    
    @Test
    @DisplayName("Test validateLineParameter with invalid line parameter")
    void testValidateLineParameterWithInvalidLine() {
      try {
        validityManager.validateLineParameter(null);
        fail("Should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Line cannot be null", e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test validateDepartureTimeParameter with valid departure time parameter")
    void testValidateDepartureTimeParameterWithValidDepartureTime() {
      try {
        validityManager.validateDepartureTimeParameter(LocalTime.of(12, 0));
      } catch (IllegalArgumentException e) {
        fail("Should not throw illegal argument exception");
      }
    }
    
    @Test
    @DisplayName("Test validateDepartureTimeParameter with invalid departure time parameter")
    void testValidateDepartureTimeParameterWithInvalidDepartureTime() {
      try {
        validityManager.validateDepartureTimeParameter(LocalTime.of(24, 0));
        fail("Should throw illegal argument exception");
      } catch (DateTimeException e) {
        assertEquals("Invalid value for HourOfDay (valid values 0 - 23): 24",
            e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test validateDelayParameter with valid delay parameter")
    void testValidateTrainNumberParameterWithValidDelay() {
      try {
        validityManager.validateDelayParameter(LocalTime.of(0, 0), LocalTime.of(0, 0));
      } catch (IllegalArgumentException e) {
        fail("Should not throw illegal argument exception");
      }
    }
    
    @Test
    @DisplayName("Test validateDelayParameter with invalid delay parameter")
    void testValidateTrainNumberParameterWithInvalidDelay() {
      try {
        validityManager.validateDelayParameter(LocalTime.of(24, 0), LocalTime.of(0, 0));
        fail("Should throw illegal argument exception");
      } catch (DateTimeException e) {
        assertEquals("Invalid value for HourOfDay (valid values 0 - 23): 24",
            e.getMessage());
      }
    }
    
    @Test
    @DisplayName("Test validateTrackParameter with valid track parameter")
    void testValidateTrackParameterWithValidTrack() {
      try {
        validityManager.validateTrackParameter(1, LocalTime.of(0, 0), LocalTime.of(0, 0));
      } catch (IllegalArgumentException e) {
        fail("Should not throw illegal argument exception");
      }
    }
    
    @Test
    @DisplayName("Test validateTrackParameter with invalid track parameter")
    void testValidateTrackParameterWithInvalidTrack() {
      try {
        validityManager.validateTrackParameter(-4, LocalTime.of(0, 0), LocalTime.of(0, 0));
        fail("Should throw illegal argument exception");
      } catch (IllegalArgumentException e) {
        assertEquals("Invalid track number: -4", e.getMessage());
      }
    }
    @Nested
    @DisplayName("Getter methods")
    class GetterMethods {
      
      private ValidityManager validityManager;
      
      @BeforeEach
      void setUp() {
        validityManager = new ValidityManager();
      }
      
        @Test
        @DisplayName("Test getOccupiedTracks")
        void testGetOccupiedTracks() {
          validityManager.getOccupiedTracks().put(LocalTime.of(0, 0), 1);
          assertNotNull(validityManager.getOccupiedTracks());
          assertEquals(1, validityManager.getOccupiedTracks().size());
        }
        
        @Test
        @DisplayName("Test getTrainNumbers")
        void testGetTrainNumbers() {
          validityManager.getTrainNumbers().add(1);
          assertNotNull(validityManager.getTrainNumbers());
          assertEquals(1, validityManager.getTrainNumbers().size());
        }
        
        @Test
        @DisplayName("Test getDestinations")
        void testGetDestinations() {
          validityManager.getDestinations().add("Oslo");
          assertNotNull(validityManager.getDestinations());
          assertEquals(1, validityManager.getDestinations().size());
        }
    }
    
    @Nested
    @DisplayName("Methods for checking and mutating collections")
    class Collections{
      
      private ValidityManager validityManager;
      
      @BeforeEach
      void setUp() {
        validityManager = new ValidityManager();
      }
      
      @Test
      @DisplayName("Test removeTrack with valid parameters")
      void testRemoveTrack() {
        validityManager.addOccupiedTrack(LocalTime.of(0, 0), 1);
        validityManager.removeOccupiedTrack(LocalTime.of(0, 0), 1);
        assertEquals(0, validityManager.getOccupiedTracks().size());
      }
      
      @Test
      @DisplayName("Test checkIfTrackIsOccupied with valid parameters")
      void testCheckIfTrackIsOccupied() {
        validityManager.addOccupiedTrack(LocalTime.of(5, 30), 1);
        assertTrue(validityManager.checkIfTrackIsOccupied(1, LocalTime.of(5, 28),
            LocalTime.of(5, 32)));
      }
      
      @Test
      @DisplayName("Test checkTrainNumberIsTaken with valid train number parameter")
      void testCheckTrainNumberIsTaken() {
        validityManager.addTrainNumber(1);
        assertTrue(validityManager.checkTrainNumberIsTaken(1));
      }
      
      @Test
      @DisplayName("Test checkIfDestinationIsInUse with valid destination parameter")
      void testCheckIfDestinationIsInUse() {
        validityManager.addDestination("Oslo");
        assertTrue(validityManager.checkIfDestinationIsInUse("Oslo"));
      }
      
      @Test
      @DisplayName("Test addTrainNumber with valid train number parameter")
      void testAddTrainNumber() {
        validityManager.addTrainNumber(1);
        assertTrue(validityManager.getTrainNumbers().contains(1));
      }
      
      @Test
      @DisplayName("Test addDestination with valid destination parameter")
      void testAddDestination() {
        validityManager.addDestination("Oslo");
        assertTrue(validityManager.getDestinations().contains("Oslo"));
      }
      
      @Test
      @DisplayName("Test addOccupiedTrack with valid parameters")
      void testAddOccupiedTrack() {
        validityManager.addOccupiedTrack(LocalTime.of(0, 0), 1);
        assertTrue(validityManager.getOccupiedTracks().containsKey(LocalTime.of(0, 0)));
        assertTrue(validityManager.getOccupiedTracks().containsValue(1));
      }
      
    }
  }
}
