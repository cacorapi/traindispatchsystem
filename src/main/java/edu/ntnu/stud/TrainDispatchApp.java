package edu.ntnu.stud;

/**
 * Main class for the Train Dispatch App.
 * Starts the UserInterface.
 *
 * @since 2.3
 * @version 3.3
 * @author 10058
 */
public class TrainDispatchApp {
  
  /**
   * Main method for the Train Dispatch App.
   * Starts the UserInterface.
   *
   * @param args Command line arguments.
   */
  public static void main(String[] args) {
    UserInterface.init();
    UserInterface.start();
    UserInterface.run();
  }
}
