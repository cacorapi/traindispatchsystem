package edu.ntnu.stud;

/**
 * Enum representing the different train lines.
 * Used in the {@see TrainDeparture} class to set the train line.
 * Also used in the {@see ValidityManager} class to ensure that the train line is valid.
 *
 * @since 2.1
 * @version 3.3
 * @author 10058
 */

public enum TrainLines {
    /** TrainLine L1 */
    L1,
    
    /** TrainLine L2 */
    L2,
    
    /** TrainLine L3 */
    L3,
    
    /** TrainLine L4 */
    L4,
    
    /** TrainLine L5 */
    L5,
    
    /** TrainLine F1 */
    F1,
    
    /** TrainLine F2 */
    F2,
    
    /** TrainLine F3 */
    F3,
    
    /** TrainLine F4 */
    F4,
    
    /** TrainLine F5 */
    F5
}
