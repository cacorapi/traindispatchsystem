package edu.ntnu.stud;

import java.time.DateTimeException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Set;



/**
 * Class used to validate parameters in the {@see TrainDeparture} class.
 * Also used to store information about occupied tracks, train numbers and destinations.
 *
 * GitHub Copilot used to generate structure of similar methods
 *
 * @since 2.3
 * @version 3.3
 * @see TrainDeparture
 */
public class ValidityManager {
  
  private static final int MAX_HOURS = 23;
  private static final int MAX_MINUTES = 59;
  private static final int ZERO = 0;
  private static final LocalTime NO_DELAY = LocalTime.of(0, 0);
  
  private final ArrayList<Integer> trainNumbers = new ArrayList<>();
  private final ArrayList<String> destinations = new ArrayList<>();
  private final HashMap<LocalTime, Integer> occupiedTracks = new HashMap<>();
  
  /**
   * Constructor for the ValidityManager class.
   * <p>
   *   Calls the {@see ValidityManager#resetCollections()} method, which resets all collections.
   *   This is done to ensure that the collections are empty when the program starts.
   *   The validity manager is only instantiated once, in the init() method in the
   *   {@see UserInterface} class so this is not a problem.
   *   </p>
   */
  public ValidityManager() {
    resetCollections();
  }
  
  /**
   * Method used to reset all collections.
   */
  public void resetCollections() {
    getOccupiedTracks().clear();
    getTrainNumbers().clear();
    getDestinations().clear();
  }
  
  /**
   * Method used to validate the current time in the program.
   * <p>
   *   Takes the parameter:
   *   <ul>
   *     <li>newTime: LocalTime</li>
   *     <li>previousTime: LocalTime</li>
   *     </ul>
   *
   *
   * @param newTime -> the new time being set
   * @param previousTime -> the previous time set
   * @throws DateTimeException if the time parameter is invalid (null, before 00:00,
   *                          after 23:59, or before the previous time)
   */
  public void validateTimeParameter(LocalTime newTime, LocalTime previousTime)
      throws DateTimeException {
    if (newTime == null || newTime.isBefore(LocalTime.of(ZERO, ZERO)) || newTime.isAfter(LocalTime
        .of(MAX_HOURS, MAX_MINUTES))) {
      throw new DateTimeException("Departure time " + newTime + " is invalid, "
          + "it must be between 00:00 and 23:59");
    } else if (newTime.isBefore(previousTime)) {
      throw new DateTimeException("Departure time " + newTime + " is invalid, "
          + "it must be after the previous departure time " + previousTime);
    }
  }
  
  /**
   * Method used to validate the train number parameter.
   * <p>
   *   Takes the following parameters:
   *   <ul>
   *     <li>trainNumber: int</li>
   *     <li>context: String</li>
   *     </ul>
   *
   *
   * @param trainNumber -> the train number to validate
   * @param context -> the context in which the method is called. If the context is "initial",
   *                 the method will check if the train number is already taken. If the context
   *                 is "later", the method will check if the train number is not taken.
   * @throws IllegalArgumentException if the train number parameter is invalid (less than or equal
   *                                  to 0, or if the train number is already taken)
   */
  public void validateTrainNumberParameter(int trainNumber, String context)
      throws IllegalArgumentException {
    if (trainNumber <= ZERO) {
      throw new IllegalArgumentException("Train number cannot be less than or equal to 0");
    } else if (context.equalsIgnoreCase("initial") && checkTrainNumberIsTaken(trainNumber)) {
      throw new IllegalArgumentException("Train with number " + trainNumber + " already exists");
    } else if (context.equalsIgnoreCase("later") && !checkTrainNumberIsTaken(trainNumber)) {
      throw new IllegalArgumentException("Train with number " + trainNumber + " does not exist");
    }
  }
  
  /**
   * Method used to validate the destination parameter.
   * <p>
   *   Takes the following parameters:
   *   <ul>
   *     <li>destination: String</li>
   *     <li>context: String</li>
   *     </ul>
   *
   *
   * @param destination -> the destination to validate
   * @param context -> the context in which the method is called. If the context is "initial",
   *                 the method will check if the destination is already in use. If the context
   *                 is "later", the method will check if the destination is not in use.
   * @throws IllegalArgumentException if the destination parameter is invalid (null or blank,
   *                                  or if the destination is already in use)
   */
  public void validateDestinationParameter(String destination, String context)
      throws IllegalArgumentException {
    if (destination.isEmpty() || destination.isBlank()) {
      throw new IllegalArgumentException("Destination cannot be null or blank");
    } else if (context.equalsIgnoreCase("later") && !checkIfDestinationIsInUse(destination)) {
      throw new IllegalArgumentException("Train with destination " + destination
          + " does not exist");
    }
  }
  
  /**
   * Method used to validate the line parameter.
   * <p>
   *   Takes the parameter:
   *   <ul>
   *     <li>line: TrainLines</li>
   *     </ul>
   *
   *
   * @param line -> the line to validate
   * @throws IllegalArgumentException if the line parameter is invalid (null, or if the line
   *                                  does not run through this train station)
   */
  public void validateLineParameter(TrainLines line) throws IllegalArgumentException {
    if (line == null) {
      throw new IllegalArgumentException("Line cannot be null");
    }
    Set<TrainLines> validLines = EnumSet.allOf(TrainLines.class);
    
    if (!validLines.contains(line)) {
      throw new IllegalArgumentException("Line " + line + " is not a valid line or "
          + "does not run through this train station");
    }
  }
  
  /**
   * Method used to validate the departure time parameter.
   * <p>
   *   Takes the parameter:
   *   <ul>
   *     <li>departureTime: LocalTime</li>
   *     </ul>
   *
   *
   * @param departureTime -> the departure time to validate
   * @throws DateTimeException if the departure time parameter is invalid (null, before 00:00,
   *                           or after 23:59)
   */
  public void validateDepartureTimeParameter(LocalTime departureTime)
      throws DateTimeException {
    if (departureTime.isBefore(LocalTime.of(ZERO, ZERO)) || departureTime.isAfter(LocalTime
        .of(MAX_HOURS, MAX_MINUTES))) {
      throw new DateTimeException("Departure time " + departureTime + " is invalid, "
          + "it must be between 00:00 and 23:59");
    }
  }
  
  /**
   * Method used to validate the delay parameter.
   * <p>
   *   Takes the following parameters:
   *   <ul>
   *     <li>delay: LocalTime</li>
   *     <li>previousDepartureTime: LocalTime</li>
   *     </ul>
   *
   *
   * @param delay -> the delay being added to the departure time
   * @param previousDepartureTime -> the previous departure time the delay will be added to
   * @throws DateTimeException if the delay parameter is invalid (null, before 00:00,
   *                           after 23:59, or will cause the departure time to exceed 23:59
   */
  public void validateDelayParameter(LocalTime delay, LocalTime previousDepartureTime)
      throws IllegalArgumentException, DateTimeException {
    if (delay == null || delay.isBefore(NO_DELAY) || delay.isAfter(LocalTime.of(MAX_HOURS,
        MAX_MINUTES))) {
      throw new IllegalArgumentException("The delay must be greater than or equal to 0");
    }
    long totalDelayMinutes = delay.getHour() * 60 + delay.getMinute();
    long totalDepartureMinutes = previousDepartureTime.getHour() * 60
        + previousDepartureTime.getMinute();
    long updatedDepartureMinutes = totalDepartureMinutes + totalDelayMinutes;
    
    if (updatedDepartureMinutes > 23 * 60 + 59) {
      throw new DateTimeException("The delay " + delay
          + " cannot make departure time exceed 23:59");
    }
  }
  
  /**
   * Method used to validate the track parameter.
   * <p>
   *   Takes the following parameters:
   *   <ul>
   *     <li>track: int</li>
   *     <li>occupiedTrackLowerLimit: LocalTime</li>
   *     <li>occupiedTrackUpperLimit: LocalTime</li>
   *     </ul>
   *
   *
   * @param track -> the track to validate
   * @param occupiedTrackLowerLimit -> the lower limit of the time interval in which the track
   *                                 is occupied is the initial departure time minus 2 minutes
   * @param occupiedTrackUpperLimit -> the upper limit of the time interval in which the track
   *                                 is occupied is the delayed departure time plus 2 minutes
   * @throws IllegalArgumentException if the track parameter is invalid (less than 0, or if the
   *                                  track is already occupied)
   */
  public void validateTrackParameter(int track, LocalTime occupiedTrackLowerLimit, LocalTime
      occupiedTrackUpperLimit) throws IllegalArgumentException {
    if (track < ZERO) {
      throw new IllegalArgumentException("Invalid track number: " + track);
    } else if (checkIfTrackIsOccupied(track, occupiedTrackLowerLimit,
        occupiedTrackUpperLimit)) {
      throw new IllegalArgumentException("Track " + track + " is already occupied");
    }
  }
  
  /**
   * Method used to remove an occupied track from the occupied tracks collection.
   * <p>
   *   It ensures that the track will be seen as available at the time that the train
   *   was originally departing at.
   *   The track may still be considered occupied at a different time
   *   </p>
   * <p>
   *    Takes the parameter:
   *    <ul>
   *      <li>departureTime: LocalTime</li>
   *      </ul>
   *
   *
   * @param departureTime -> the departure time the delay will be added to
   * @param track -> the track to remove from the occupied tracks collection
   * @throws DateTimeException if the delayed departure time parameter is invalid (null, before
   *                           00:00, after 23:59, or before the departure time)
   */
  public void removeOccupiedTrack(LocalTime departureTime, int track) {
    getOccupiedTracks().remove(departureTime, track);
  }
  
  /**
   * Method used to check if the track is occupied.
   * <p>
   *   Takes the following parameters:
   *   <ul>
   *     <li>track: int</li>
   *     <li>lowerTimeLimit: LocalTime</li>
   *     <li>upperTimeLimit: LocalTime</li>
   *     </ul>
   *
   *
   * @param track -> the track to check
   * @param lowerTimeLimit -> the lower limit of the time interval in which the track
   *                        is occupied is the initial departure time minus 2 minutes
   * @param upperTimeLimit -> the upper limit of the time interval in which the track
   *                        is occupied is the delayed departure time plus 2 minutes
   * @return true if the track is occupied, false if not
   */
  public boolean checkIfTrackIsOccupied(int track, LocalTime lowerTimeLimit,
                                              LocalTime upperTimeLimit) {
    return getOccupiedTracks().keySet().stream()
        .filter(departureTime -> departureTime.isAfter(lowerTimeLimit)
            && departureTime.isBefore(upperTimeLimit))
        .anyMatch(departureTime -> getOccupiedTracks().get(departureTime) == track);
  }
  
  /**
   * Method used to check if the train number is already taken.
   * <p>
   *   Takes the parameter:
   *   <ul>
   *     <li>trainNumber: int</li>
   *     </ul>
   *
   *
   * @param trainNumber -> the train number to check
   * @return true if the train number is already taken, false if not
   */
  public boolean checkTrainNumberIsTaken(int trainNumber) {
    return getTrainNumbers().contains(trainNumber);
  }
  
  /**
   * Method used to check if the destination is already in use.
   *  <p>
   *    Takes the parameter:
   *    <ul>
   *      <li>destination: String</li>
   *      </ul>
   *
   *
   * @param destination -> the destination to check
   * @return true if the destination is already in use, false if not
   */
  public boolean checkIfDestinationIsInUse(String destination) {
    return getDestinations().contains(destination);
  }
  
  /**
   * Accessor (getter) method used to get the train numbers collection.
   *
   * @return the train numbers collection
   */
  public ArrayList<Integer> getTrainNumbers() {
    return trainNumbers;
  }
  
  /**
   * Accessor (getter) method used to get the destinations collection.
   *
   * @return the destinations collection
   */
  public ArrayList<String> getDestinations() {
    return destinations;
  }
  
  /**
   * Accessor (getter) method used to get the occupied tracks collection.
   *
   * @return the occupied tracks collection
   */
  public HashMap<LocalTime, Integer> getOccupiedTracks() {
    return occupiedTracks;
  }
  
  /**
   * Mutator method used to add a train number to the train numbers collection.
   * <p>
   *   Takes the parameter:
   *   <ul>
   *     <li>trainNumber: int</li>
   *     </ul>
   *
   *
   * @param trainNumber -> the train number to add to the train numbers collection
   */
  public void addTrainNumber(int trainNumber) {
    getTrainNumbers().add(trainNumber);
  }
  
  /**
   * Mutator method used to add a destination to the destinations collection.
   * <p>
   *   Takes the parameter:
   *   <ul>
   *     <li>destination: String</li>
   *     </ul>
   *
   *
   * @param destination -> the destination to add to the destinations collection
   */
  public void addDestination(String destination) {
    getDestinations().add(destination);
  }
  
  /**
   * Mutator method used to add an occupied track to the occupied tracks collection.
   * <p>
   *   Takes the following parameters:
   *   <ul>
   *     <li>departureTime: LocalTime</li>
   *     <li>track: int</li>
   *     </ul>
   *
   *
   * @param departureTime -> the departure time of the train departure
   * @param track -> the track of the train departure
   */
  public void addOccupiedTrack(LocalTime departureTime, int track) {
    getOccupiedTracks().put(departureTime, track);
  }
}

