package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;

/**
 * Class that contains and controls the information for a train departure,
 * including the train number, destination, train line, departure time, delay, track, and
 * delayed departure time.
 * <p>
 *   Class includes methods to set and retrieve information about the train departure.
 *   It also interacts with a {@see ValidityManager} for validation of input parameters
 *   and is used in the {@see TrainDepartureRegister} class where each instance of
 *   TrainDeparture is a registered departure.
 *   GitHub Copilot used to generate parts of code - particularly getters and setters.
 *   </p>
 *
 * @since 2.0
 * @version 3.3
 * @author 10058
 */

public class TrainDeparture {
  
  private static final int UNASSIGNED_TRACK_NUMBER = -1;
  private static final LocalTime NO_DELAY = LocalTime.of(0, 0);
  private static final int ZERO = 0;
  
  private final int trainNumber;
  private final String destination;
  private final TrainLines line;
  private final LocalTime departureTime;
  private int track;
  private LocalTime delayedDepartureTime;
  private LocalTime delay;
  
  ValidityManager validityManager;
  
  /**
   * Constructor for the TrainDeparture class.
   * <p>
   *   Takes the following parameters:
   *   <ul>
   *     <li>trainNumber: int</li>
   *     <li>destination: String</li>
   *     <li>line: TrainLines</li>
   *     <li>track: int</li>
   *     <li>departureTime: LocalTime</li>
   *     <li>delay: LocalTime</li>
   *     <li>validityManager: ValidityManager</li>
   *     </ul>
   *
   * <p>
   *   The constructor validates the input parameters using the {@see ValidityManager} class.
   *   If the parameters are valid, the constructor sets the parameters to the corresponding
   *   instance variables. Final variables are set in the constructor and cannot be changed.
   *   Track and delay are not final, so they are set using the corresponding setter methods.
   *   </p>
   *
   * @param trainNumber -> the train number
   * @param destination -> the destination
   * @param line -> the train line
   * @param track -> the initial track
   * @param departureTime -> the departure time
   * @param delay -> the initial delay
   * @param validityManager -> the validity manager
   */
  public TrainDeparture(int trainNumber, String destination, TrainLines line, int track,
                        LocalTime departureTime, LocalTime delay, ValidityManager validityManager) {
    
    validityManager.validateTrainNumberParameter(trainNumber, "initial");
    validityManager.validateDestinationParameter(destination, "initial");
    validityManager.validateLineParameter(line);
    validityManager.validateDepartureTimeParameter(departureTime);
    
    this. validityManager = validityManager;
    
    this.trainNumber = trainNumber;
    validityManager.addTrainNumber(trainNumber);
    
    this.destination = destination;
    validityManager.addDestination(destination);
    
    this.line = line;
    this.departureTime = departureTime;
    
    setDelayedDepartureTime(delay, "initial");
    setDelay(delay, "initial");
    setTrack(track);
  }
  
  /**
   * Mutator (setter) method for delay.
   * <p>
   *   Takes the following parameters:
   *   <ul>
   *     <li>delay: LocalTime</li>
   *     <li>context: String</li>
   *     <li>line: TrainLines</li>
   *     <li>departureTime: LocalTime</li>
   *     <li>validityManager: ValidityManager</li>
   *     </ul>
   *
   * <p>
   *   The method validates the input parameters using the {@see ValidityManager} class.
   *   If the parameters are valid, the method sets the parameters to the corresponding
   *   instance variables.
   *   </p>
   *
   * @param delay -> the delay to be set
   * @param context -> "initial" or "later" depending on whether the delay is being set as part of
   *                initialization
   */
  public void setDelay(LocalTime delay, String context) {
    if (context.equalsIgnoreCase("initial")) {
      validityManager.validateDelayParameter(delay, getDepartureTime());
      if (delay.equals(NO_DELAY)) {
        this.delay = NO_DELAY;
      } else {
        this.delay = delay;
        setDelayedDepartureTime(delay, "initial");
      }
    } else if (context.equalsIgnoreCase("later")) {
      validityManager.validateDelayParameter(delay, getDelayedDepartureTime());
      if (delay.equals(NO_DELAY)) {
        this.delay = NO_DELAY;
        setDelayedDepartureTime(delay, "later");
      } else {
        this.delay = getDelay().plus(Duration.ofMinutes(delay.getMinute())
            .plusHours(delay.getHour()));
        setDelayedDepartureTime(delay, "later");
      }
    }
  }
  
  /**
   * Mutator (setter) method for delayedDepartureTime.
   * <p>
   *   Takes the following parameters:
   *   <ul>
   *     <li>delay: LocalTime</li>
   *     <li>context: String</li>
   *     <li>line: TrainLines</li>
   *     <li>departureTime: LocalTime</li>
   *     <li>validityManager: ValidityManager</li>
   *     </ul>
   *     </p>
   * <p>
   *   The method validates the input parameters using the {@see ValidityManager} class.
   *   If the parameters are valid, the method sets the parameters to the corresponding
   *   instance variables.
   *   </p>
   *
   * @param delay -> the delay to be set
   * @param context -> "initial" or "later" depending on whether the delay is being added as part
   *                of initialization
   */
  private void setDelayedDepartureTime(LocalTime delay, String context) {
    if (context.equalsIgnoreCase("initial")) {
      validityManager.validateDelayParameter(delay, getDepartureTime());
      if (delay.equals(NO_DELAY)) {
        this.delayedDepartureTime = getDepartureTime();
      } else {
        this.delayedDepartureTime = getDepartureTime().plus(Duration.ofMinutes(delay.getMinute())
            .plusHours(delay.getHour()));
      }
    } else if (context.equalsIgnoreCase("later")) {
      validityManager.validateDelayParameter(delay, getDelayedDepartureTime());
      if (delay.equals(NO_DELAY)) {
        this.delayedDepartureTime = getDepartureTime();
      } else {
        this.delayedDepartureTime = getDelayedDepartureTime().plus(Duration
            .ofMinutes(delay.getMinute()).plusHours(delay.getHour()));
      }
    }
  }
  
  /**
   * Mutator (setter) method for track.
   * <p>
   *   Takes the following parameters:
   *   <ul>
   *     <li>track: int</li>
   *     <li>occupiedTrackLowerLimit: LocalTime</li>
   *     <li>occupiedTrackUpperLimit: LocalTime</li>
   *     <li>validityManager: ValidityManager</li>
   *     </ul>
   *
   * <p>
   *   The method validates the input parameters using the {@see ValidityManager} class
   *   where the occupiedTrackLowerLimit and occupiedTrackUpperLimit are calculated
   *   based on the original departure time and the delayed departure time, with an
   *   2 min of leeway on either side
   *   If the parameters are valid, the method sets the parameters to the corresponding
   *   instance variables.
   *   </p>
   *
   * @param track -> the track to be set
   */
  public void setTrack(int track) {
    LocalTime occupiedTrackLowerLimit = getDepartureTime().minusMinutes(2);
    LocalTime occupiedTrackUpperLimit = getDelayedDepartureTime().plusMinutes(2);
    
    validityManager.validateTrackParameter(track, occupiedTrackLowerLimit, occupiedTrackUpperLimit);
    
    if (track == ZERO || track == UNASSIGNED_TRACK_NUMBER) {
      this.track = UNASSIGNED_TRACK_NUMBER;
    } else {
      this.track = track;
      
      validityManager.addOccupiedTrack(getDepartureTime(), track);
    }
  }
  
  /**
   * Mutator method which removes track, i.e. sets track to -1.
   * <p>
   *   The method also removes the track from the validityManager HashMap OccupiedTracks.
   *   </p>
   */
  public void removeTrack() {
    validityManager.removeOccupiedTrack(getDepartureTime(), getTrack());
    this.track = UNASSIGNED_TRACK_NUMBER;
  }
  
  /**
   * Accessor (getter) method for trainNumber.
   *
   * @return the train number
   */
  public int getTrainNumber() {
    return trainNumber;
  }
  
  /**
   * Accessor (getter) method for destination.
   *
   * @return the destination
   */
  public String getDestination() {
    return destination;
  }
  
  /**
   * Accessor (getter) method for line.
   *
   * @return the train line
   */
  public TrainLines getLine() {
    return line;
  }
  
  /**
   * Accessor (getter) method for track.
   *
   * @return the track
   */
  public int getTrack() {
    return track;
  }
  
  /**
   * Accessor (getter) method for departureTime.
   *
   * @return the departure time
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }
  
  /**
   * Accessor (getter) method for delay.
   *
   * @return the delay
   */
  public LocalTime getDelayedDepartureTime() {
    return delayedDepartureTime;
  }
  
  /**
   * Accessor (getter) method for delay.
   *
   * @return the delay
   */
  public LocalTime getDelay() {
    return delay;
  }
  
  /**
   * toString method returns a string representation of the object.
   * <p>
   *   The string representation consists of the following information in a wrap text format:
   *   <ul>
   *     <li>Train number: int</li>
   *     <li>Destination: String</li>
   *     <li>Line: TrainLines</li>
   *     <li>Track: int</li>
   *     <li>Departure time: LocalTime</li>
   *     <li>Delay: LocalTime</li>
   *     <li>Delayed departure time: LocalTime</li>
   *     </ul>
   *
   * <p>
   *   If there is no delay, departure time is blank.
   *   If no track is assigned, track is blank.
   *   </p>
   *
   * @return a string representation of the object
   */
  public String toString() {
    if (delay.equals(NO_DELAY)) {
      if (getTrack() == UNASSIGNED_TRACK_NUMBER) {
        return String.format("Train number: %d%nDestination: %s%nLine: %s%nTrack: %s%nDeparture"
                + " Time: %s%nDelay: %s", getTrainNumber(), getDestination(), getLine(), "",
            getDepartureTime(), getDelay());
      }
      return String.format("Train number: %d%nDestination: %s%nLine: %s%nTrack: %d%nDeparture"
          + " Time: %s%nDelay: %s", getTrainNumber(), getDestination(), getLine(),
          getTrack(), getDepartureTime(), getDelay());
    }
    if (getTrack() == UNASSIGNED_TRACK_NUMBER) {
      return String.format("Train number: %d%nDestination: %s%nLine: %s%nTrack: %s%nDeparture"
              + " Time: %s%nDelay: %s%nDelayed Departure Time: %s%n",
          getTrainNumber(), getDestination(), getLine(), "", getDepartureTime(),
          getDelay(), getDelayedDepartureTime());
    }
    return String.format("Train number: %d%nDestination: %s%nLine: %s%nTrack: %d%nDeparture Time: "
            + "%s%nDelay: %s%nDelayed Departure Time: %s%n",
        getTrainNumber(), getDestination(), getLine(), getTrack(), getDepartureTime(), getDelay(),
        getDelayedDepartureTime());
  }
  
  /**
   * toString method returns a string representation of the object.
   * <p>
   *   The string representation consists of the following information in a single line text format:
   *   <ul>
   *     <li>Train number: int</li>
   *     <li>Destination: String</li>
   *     <li>Line: TrainLines</li>
   *     <li>Track: int</li>
   *     <li>Departure time: LocalTime</li>
   *     <li>Delay: LocalTime</li>
   *     <li>Delayed departure time: LocalTime</li>
   *     </ul>
   *
   * <p>
   *   If there is no delay, departure time is blank.
   *   If no track is assigned, track is blank.
   *   </p>
   *
   * @return a string representation of the object
   */
  public String toSingleLineString() {
    if (delay.equals(NO_DELAY)) {
      if (getTrack() == UNASSIGNED_TRACK_NUMBER) {
        return String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
            getTrainNumber(), getDestination(), getLine(), "", getDepartureTime(),
            getDelay(), "");
      }
      return String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
          getTrainNumber(), getDestination(), getLine(), getTrack(), getDepartureTime(),
          getDelay(), "");
    }
    if (getTrack() == UNASSIGNED_TRACK_NUMBER) {
      return String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s | ",
          getTrainNumber(), getDestination(), getLine(), "", getDepartureTime(),
          getDelay(), getDelayedDepartureTime());
    }
    return String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s | ",
        getTrainNumber(), getDestination(), getLine(), getTrack(),
        getDepartureTime(), getDelay(), getDelayedDepartureTime());
  }
}