package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class for the UserInterface.
 * Contains the menu and the main method.
 * Starts the program.
 *
 * GitHub Copilot used to generate parts of code - particularly the structure for the switch-case.
 *
 * @since 2.3
 * @version 3.3
 * @author 10058
 */
public class UserInterface {
  private static final int CHANGE_TIME = 1;
  private static final int DISPLAY_DEPARTURE_TABLE = 2;
  private static final int REGISTER_NEW_TRAIN_DEPARTURE = 3;
  private static final int ASSIGN_TRACK = 4;
  private static final int REASSIGN_TRACK = 5;
  private static final int ADD_DELAY_TO_TRAIN = 6;
  private static final int FIND_TRAIN_BASED_ON_TRAIN_NUMBER = 7;
  private static final int FIND_TRAINS_BASED_ON_DESTINATION = 8;
  private static final int EXIT_PROGRAM = 9;
  
  static ValidityManager validityManager;
  private static TrainDepartureRegister trainDepartureRegister;
  private static TrainLines[] trainLines;
  private static Scanner scanner;
  private static LocalTime currentTime;
  
  /**
   * Static method containing the user menu as a String.
   * Allows the user to perform any of the below actions by entering the corresponding number.
   * Actions:
   * <ul>
   *   <li>Change time</li>
   *   <li>Display departure table</li>
   *   <li>Register new train departure</li>
   *   <li>Assign track</li>
   *   <li>Reassign track</li>
   *   <li>Add delay to train</li>
   *   <li>Find train based on train number</li>
   *   <li>Find trains based on destination</li>
   *   <li>Exit program</li>
   *   </ul>
   *
   * @return String containing the menu.
   */
  public static String menu() {
    return "-------------------------------------------------------"
        + "-----------------------------------------------------------------------------------\n"
        + String.format("\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n",
        "Menu", "Current time: " +  trainDepartureRegister.getTime(), "1. Set time",
        "2. Display departure table.", "3. Register new train departure.", "4. Assign track.",
        "5. Reassign track.", "6. Add delay to train.", "7. Find train based on train number.",
        "8. Find trains based on destination.", "9. Exit program.") + "\n----------------------"
        + "--------------------------------------------------------------------------------------"
        + "------------------------------";
  }
  
  /**
   * Static method for initializing the program.
   * <p>
   *   Initializes the following:
   *   <ul>
   *     <li>time: localTime</li>
   *     <li>ValidityManager: ValidityManager</li>
   *     <li>TrainDepartureRegister: TrainDepartureRegister</li>
   *     <li>TrainLines: TrainLines[]</li>
   *     <li>Scanner: Scanner</li>
   *     </ul>
   *
   */
  public static void init() {
    currentTime = LocalTime.of(0, 0);
    validityManager = new ValidityManager();
    trainDepartureRegister = new TrainDepartureRegister(currentTime, validityManager);
    trainLines = TrainLines.values();
    scanner = new Scanner(System.in);
  }
  
  /**
   * Static method for starting the program.
   * <p>
   *   Adds some train departures to the TrainDepartureRegister.
   *   Prints the TrainDepartureRegister.
   *   </p>
   */
  public static void start() {
    trainDepartureRegister.addTrainDeparture(113, "Barcelona", trainLines[1],
        0, LocalTime.of(10, 30), LocalTime.of(0, 0));
    trainDepartureRegister.addTrainDeparture(114,  "London", trainLines[5],
        2, LocalTime.of(11, 45), LocalTime.of(0, 0));
    trainDepartureRegister.addTrainDeparture(115,  "Rome", trainLines[2],
        3, LocalTime.of(12, 0), LocalTime.of(0, 0));
    trainDepartureRegister.addTrainDeparture(116,  "Paris", trainLines[7],
        4, LocalTime.of(13, 15), LocalTime.of(0, 30));
    
    System.out.println("\n" + trainDepartureRegister);
  }
  
  /**
   * Static method for running the program.
   * <p>
   *   Contains the main loop of the program with the following functions:
   *   <ul>
   *     <li>Prints the menu</li>
   *     <li>Gets the user's choice</li>
   *     <li>Performs the corresponding action</li>
   *     <li>Prints the result of the action</li>
   *     <li>Repeats until the user enters 9</li>
   *     </ul>
   *
   *     Additionally:
   *     <ul>
   *       <li>Reprints the menu after each action, whether successful or not.</li>
   *       <li>Repeats until the user enters a valid choice.</li>
   *       <li>Catches exceptions and prints a message to the user.</li>
   *       <li>Validates the user's input and prints a message to the user if the
   *       input is invalid.</li>
   *       </ul>
   *
   */
  public static void run() {
    System.out.println("\n------------------------------------------------------------------"
        + "------------------------------------------------------------------------");
    System.out.println();
    System.out.println("Welcome to the train dispatch system operator.");
    System.out.println();
    int choice;
    do {
      System.out.println(menu());
      try {
        choice = scanner.nextInt();
      } catch (Exception e) {
        if (e instanceof InputMismatchException) {
          System.out.println("Invalid input, check your input type or formatting. "
              + "\nPlease try again.");
        } else {
          System.out.println("Unexpected error: " + e.getMessage() + ". \nPlease try again.");
        }
        choice = 0;
      }
      System.out.println();
      
      switch (choice) {
        case CHANGE_TIME:
          try {
            System.out.println("To set the current time, please enter the time "
                + "in 24h format (HH:MM):");
            String time1 = scanner.next();
            String[] splitTime1 =  time1.split(":");
            int hour1 = Integer.parseInt(splitTime1[0]);
            int minute1 = Integer.parseInt(splitTime1[1]);
            validityManager.validateTimeParameter(LocalTime.of(hour1, minute1), currentTime);
            trainDepartureRegister.setTime(LocalTime.of(hour1, minute1), "later");
            currentTime = LocalTime.of(hour1, minute1);
            
            System.out.println("Time changed!");
          } catch (Exception e) {
            if (e instanceof InputMismatchException) {
              System.out.println("Invalid input, check your input type or formatting. "
                  + "\nPlease try again.");
            } else {
              System.out.println("Unexpected error: " + e.getMessage() + ". \nPlease try again.");
            }
          }
          break;
        case DISPLAY_DEPARTURE_TABLE:
          try {
            System.out.println(trainDepartureRegister.toString());
          } catch (Exception e) {
            if (e instanceof InputMismatchException) {
              System.out.println("Invalid input, check your input type or formatting. "
                  + "\nPlease try again.");
            } else {
              System.out.println("Unexpected error: " + e.getMessage() + ". \nPlease try again.");
            }
          }
          break;
        case REGISTER_NEW_TRAIN_DEPARTURE:
          try {
            System.out.println("To register a new train departure, "
                + "please enter the following details:");
            System.out.println("Train number:");
            int trainNumber3 = scanner.nextInt();
            validityManager.validateTrainNumberParameter(trainNumber3, "initial");
            
            System.out.println("Destination:");
            scanner.nextLine();
            String destination3 = scanner.nextLine();
            validityManager.validateDestinationParameter(destination3, "initial");
            
            System.out.println("Line:");
            System.out.println("Lines going to this station are "
                + "L1, L2, L3, L4, L5, F1, F2, F3, F4, F5");
            TrainLines line3 = TrainLines.valueOf(scanner.nextLine());
            validityManager.validateLineParameter(line3);
            
            System.out.println("Departure time in 24h format (HH:MM):");
            String[] splitTime3 = scanner.next().split(":");
            int hour3 = Integer.parseInt(splitTime3[0]);
            int minute3 = Integer.parseInt(splitTime3[1]);
            validityManager.validateDepartureTimeParameter(LocalTime.of(hour3, minute3));
            
            System.out.println("Delay in 24h format (HH:MM, enter 0:0 if there is no delay):");
            scanner.nextLine();
            String[] delaySplitTime3 = scanner.nextLine().split(":");
            int delayHour3 = Integer.parseInt(delaySplitTime3[0]);
            int delayMinute3 = Integer.parseInt(delaySplitTime3[1]);
            validityManager.validateDelayParameter(LocalTime.of(delayHour3, delayMinute3),
                LocalTime.of(hour3, minute3));
            
            System.out.println("Track number (enter 0 or -1 if no track is currently assigned):");
            int track = scanner.nextInt();
            validityManager.validateTrackParameter(track, LocalTime.of(hour3, minute3)
                .minusMinutes(2), LocalTime.of(hour3, minute3).plusHours(delayHour3)
                .plusMinutes(delayMinute3 + 2));
            
            trainDepartureRegister.addTrainDeparture(trainNumber3, destination3, line3, track,
                LocalTime.of(hour3, minute3), LocalTime.of(delayHour3, delayMinute3));
            System.out.println("Train departure added!");
          } catch (Exception e) {
            if (e.getMessage().contains("No enum constant")) {
              System.out.println("That line is not a valid line or does not run through this "
                  + "train station. \nPlease try again.");
            } else if (e instanceof InputMismatchException) {
              System.out.println("Invalid input, check your input type or formatting. "
                  + "\nPlease try again.");
            } else {
              System.out.println("Unexpected error: " + e.getMessage() + ". \nPlease try again.");
            }
          }
          break;
        
        case ASSIGN_TRACK:
          try {
            System.out.println("To assign a track to a train, please enter the following details:");
            System.out.println("Train number:");
            int trainNumber4 = scanner.nextInt();
            validityManager.validateTrainNumberParameter(trainNumber4, "later");
            
            System.out.println("Track number (enter 0 or -1 to assign no track):");
            int trackNumber4 = scanner.nextInt();
            validityManager.validateTrackParameter(trackNumber4, LocalTime.of(0, 0),
                LocalTime.of(23, 59));
            
            trainDepartureRegister.getTrainDepartureFromNumber(trainNumber4).setTrack(trackNumber4);
            System.out.println("Track assigned!");
          } catch (Exception e) {
            if (e instanceof InputMismatchException) {
              System.out.println("Invalid input, check your input type or formatting. "
                  + "\nPlease try again.");
            } else {
              System.out.println("Unexpected error: " + e.getMessage() + ". \nPlease try again.");
            }
          }
          break;
        
        case REASSIGN_TRACK:
          try {
            System.out.println("To reassign a track to a train, "
                + "please enter the following details:");
            System.out.println("Train number:");
            int trainNumber5 = scanner.nextInt();
            validityManager.validateTrainNumberParameter(trainNumber5, "later");
            
            System.out.println("Track number (enter 0 or -1 assign no track):");
            int trackNumber5 = scanner.nextInt();
            validityManager.validateTrackParameter(trackNumber5, LocalTime.of(0, 0),
                LocalTime.of(23, 59));
            
            trainDepartureRegister.removeTrack(trainNumber5);
            trainDepartureRegister.updateTrack(trackNumber5, trainNumber5);
            System.out.println("Track reassigned!");
          } catch (Exception e) {
            if (e instanceof InputMismatchException) {
              System.out.println("Invalid input, check your input type or formatting. "
                  + "\nPlease try again.");
            } else {
              System.out.println("Unexpected error: " + e.getMessage() + ". \nPlease try again.");
            }
          }
          break;
          
        case ADD_DELAY_TO_TRAIN:
          try {
            System.out.println("To add a delay to a train, please enter the following details:");
            System.out.println("Train number:");
            int trainNumber6 = scanner.nextInt();
            validityManager.validateTrainNumberParameter(trainNumber6, "later");
            
            System.out.println("Delay in 24h format (HH:MM):");
            String delay6 = scanner.next();
            String[] delaySplitTime6 = delay6.split(":");
            int delayHour6 = Integer.parseInt(delaySplitTime6[0]);
            int delayMinute6 = Integer.parseInt(delaySplitTime6[1]);
            
            trainDepartureRegister
                .updateDelay(LocalTime.of(delayHour6, delayMinute6), trainNumber6);
            System.out.println("Delay added!");
          } catch (Exception e) {
            if (e instanceof InputMismatchException) {
              System.out.println("Invalid input, check your input type or formatting. "
                  + "\nPlease try again.");
            } else {
              System.out.println("Unexpected error: " + e.getMessage() + ". \nPlease try again.");
            }
          }
          break;
        
        case FIND_TRAIN_BASED_ON_TRAIN_NUMBER:
          try {
            System.out.println("To search for a train based on its number, "
                + "please enter the following details");
            System.out.println("Train number:");
            int trainNumber7 = scanner.nextInt();
            validityManager.validateTrainNumberParameter(trainNumber7, "later");
            
            System.out.println(trainDepartureRegister.getTrainDepartureFromNumber(trainNumber7));
          } catch (Exception e) {
            if (e instanceof InputMismatchException) {
              System.out.println("Invalid input, check your input type or formatting. "
                  + "\nPlease try again.");
            } else {
              System.out.println("Unexpected error: " + e.getMessage() + ". \nPlease try again.");
            }
          }
          break;
        
        case FIND_TRAINS_BASED_ON_DESTINATION:
          try {
            System.out.println("To search for trains based on destination, "
                + "please enter the following details");
            System.out.println("Destination:");
            scanner.nextLine();
            String destination8 = scanner.nextLine();
            validityManager.validateDestinationParameter(destination8, "later");
            
            System.out.println(trainDepartureRegister
                .getTrainDepartureFromDestination(destination8));
          } catch (Exception e) {
            if (e instanceof InputMismatchException) {
              System.out.println("Invalid input, check your input type or formatting. "
                  + "\nPlease try again.");
            } else {
              System.out.println("Unexpected error: " + e.getMessage() + ". \nPlease try again.");
            }
          }
          break;
        
        default:
          if (choice != EXIT_PROGRAM) {
            System.out.println("Invalid choice. Please select one of the options shown.");
            try (Scanner scanner = new Scanner(System.in)) {
              choice = scanner.nextInt();
            } catch (Exception e) {
              if (e instanceof InputMismatchException) {
                System.out.println("Invalid input, check your input type or formatting. "
                    + "\nPlease try again.");
              } else {
                System.out.println("Unexpected error: " + e.getMessage() + ". \nPlease try again.");
              }
            }
          }
      }
    } while (choice != EXIT_PROGRAM);
    System.out.println("Thank you for using the train dispatch system operator. Goodbye!");
    System.exit(0);
  }
}
