package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;

/**
 * Class for storing and managing TrainDeparture objects.
 * <p>
 *   The class is responsible for storing TrainDeparture objects in a HashMap, and for sorting the
 *   HashMap by departure time. The class is also responsible for removing trains that have departed
 *   from the HashMap, and for updating the delay and track of a train. Furthermore, the class is
 *   able to return a String representation of the HashMap, all the trains with a certain
 *   destination, and a TrainDeparture object based on its train number.
 *   GitHub Copilot used to generate parts of code - particularly getters and setters.
 *   </p>
 * <p>
 *   The class is also responsible for validating the parameters of the methods, and for creating
 *   TrainDeparture objects.
 *   </p>
 *
 * @since 2.1
 * @version 3.3
 * @author 10058
 */

public class TrainDepartureRegister {
  
  private LinkedHashMap<Integer, TrainDeparture> trainDepartures;
  private LocalTime time;
  
  private final ValidityManager validityManager;
  
  /**
   * Constructor for the TrainDepartureRegister class.
   * <p>
   *   The constructor takes a LocalTime object and a ValidityManager object as parameters. The
   *   constructor is responsible for validating the LocalTime object, and for creating a
   *   LinkedHashMap object.
   *   </p>
   *
   * @param time LocalTime object representing the current time.
   * @param validityManager ValidityManager object used for validating parameters.
   */
  public TrainDepartureRegister(LocalTime time, ValidityManager validityManager) {
    this.validityManager = validityManager;
    this.trainDepartures = new LinkedHashMap<>();
    setTime(time, "initial");
  }
  
  /**
   * Mutator method sorts the HashMap by departure time.
   * <p>
   *   The method is responsible for sorting the HashMap by departure time, and for storing the
   *   sorted LinkedHashMap in a new LinkedHashMap object.
   *   </p>
   */
  public void sortTrainDeparturesByDepartureTime() {
    trainDepartures = trainDepartures.values().stream()
        .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
        .collect(Collectors.toMap(TrainDeparture::getTrainNumber, trainDeparture -> trainDeparture,
            (trainDeparture1, trainDeparture2) -> trainDeparture1, LinkedHashMap::new));
  }
  
  /**
   * Accessor (getter) method returns the HashMap  of registered train departures.
   *
   * @return HashMap object containing TrainDeparture objects.
   */
  public HashMap<Integer, TrainDeparture> getTrainDepartures() {
    return trainDepartures;
  }
  
  /**
   * Mutator (setter) method sets the time of the TrainDepartureRegister object.
   * <p>
   *   The method is responsible for validating the LocalTime object, and for removing trains that
   *   have departed.
   *   </p>
   * <p>
   *   Takes the parameter:
   *   <ul>
   *     <li>time: LocalTime</li>
   *     <li>context: String</li>
   *     </ul>
   *
   *
   * @param time -> the current time.
   * @param context -> the context (initial or not, ie. later).
   */
  public void setTime(LocalTime time, String context) {
    if (context.equals("initial")) {
      validityManager.validateTimeParameter(time, LocalTime.of(0, 0));
      this.time = time;
    } else if (context.equals("later")) {
      validityManager.validateTimeParameter(time, getTime());
      this.time = time;
    }
    validityManager.validateTimeParameter(time, getTime());
    this.time = time;
    removeDepartedTrains();
  }
  
  /**
   * Accessor (getter) method returns the time of the TrainDepartureRegister object.
   *
   * @return LocalTime object representing the current time.
   */
  public LocalTime getTime() {
    return time;
  }
  
  /**
   * Mutator method creates a TrainDeparture object and stores it in the HashMap.
   * <p>
   *   The method is responsible for validating the parameters, and for creating a TrainDeparture
   *   object. The method is also responsible for storing the TrainDeparture object in the HashMap,
   *   and for sorting the HashMap by departure time.
   *   </p>
   * <p>
   *   Takes the parameters:
   *   <ul>
   *     <li>trainNumber - int</li>
   *     <li>destination - String</li>
   *     <li>line - TrainLines</li>
   *     <li>track - int</li>
   *     <li>departureTime - LocalTime</li>
   *     <li>delay - LocalTime</li>
   *     </ul>
   *
   *
   * @param trainNumber -> the train number.
   * @param destination -> the destination.
   * @param line -> the line.
   * @param track ->  the track.
   * @param departureTime -> the departure time.
   * @param delay -> the delay.
   */
  public void addTrainDeparture(int trainNumber, String destination, TrainLines line, int track,
                                LocalTime departureTime, LocalTime delay) {
    
    TrainDeparture trainDeparture = new TrainDeparture(trainNumber, destination, line, track,
        departureTime, delay, validityManager);
    
    trainDepartures.put(trainDeparture.getTrainNumber(), trainDeparture);
    sortTrainDeparturesByDepartureTime();
  }
  
  /**
   * Accessor (getter) method returns a TrainDeparture object based on its train number.
   * <p>
   *   The method is responsible for validating the parameter, and for returning a TrainDeparture
   *   object based on its train number.
   *   </p>
   * <p>
   *   Takes the parameter:
   *   <ul>
   *     <li>trainNumber - int</li>
   *     </ul>
   *
   *
   * @param trainNumber int representing the train number.
   * @return TrainDeparture object.
   */
  public TrainDeparture getTrainDepartureFromNumber(int trainNumber) {
    validityManager.validateTrainNumberParameter(trainNumber, "later");
    
    return trainDepartures.get(trainNumber);
  }
  
  /**
   * Accessor (getter) method returns a String representation of all the trains with a certain
   * destination.
   * <p>
   *   The method is responsible for validating the parameter, and for returning a String
   *   representation of all the trains with a certain destination.
   *   </p>
   * <p>
   *   Takes the parameter:
   *   <ul>
   *     <li>destination - String</li>
   *     </ul>
   *
   *
   * @param destination String representing the destination.
   * @return String representation of all the trains with a certain destination.
   */
  public String getTrainDepartureFromDestination(String destination) {
    validityManager.validateDestinationParameter(destination, "later");
    
    HashMap<Integer, TrainDeparture> trainsSortedByDestination
        = getTrainDepartures().values().stream()
        .filter(trainDeparture -> trainDeparture.getDestination().equalsIgnoreCase(destination))
        .collect(Collectors.toMap(TrainDeparture::getTrainNumber, trainDeparture -> trainDeparture,
            (trainDeparture1, trainDeparture2) -> trainDeparture1, HashMap::new));
    
    String header = String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
        "Train", "Destination", "Line",
        "Track", "Departure Time", "Delay", "Delayed Departure Time");
    
    return header + "\n" + trainsSortedByDestination.values()
        .stream()
        .map(TrainDeparture::toSingleLineString)
        .collect(Collectors.joining("\n"));
  }
  
  /**
   * Mutator method removes trains that have departed from the HashMap.
   * <p>
   *   The method is responsible for removing trains that have departed from the HashMap.
   *   </p>
   */
  public void removeDepartedTrains() {
    getTrainDepartures().values().removeIf(trainDeparture -> trainDeparture.getDepartureTime()
        .isBefore(getTime()));
  }
  
  /**
   * Mutator method updates the delay of a train.
   * <p>
   *   The method is responsible for validating the parameters, and for updating the delay of a
   *   train.
   *   </p>
   * <p>
   *   Takes the parameters:
   *   <ul>
   *     <li>delay - LocalTime</li>
   *     <li>trainNumber - int</li>
   *     </ul>
   *
   *
   * @param delay LocalTime object representing the delay.
   * @param trainNumber int representing the train number.
   */
  public void updateDelay(LocalTime delay, int trainNumber) {
    validityManager.validateTrainNumberParameter(trainNumber, "later");
    validityManager.validateDelayParameter(delay, getTrainDepartures().get(trainNumber)
        .getDelayedDepartureTime());
    getTrainDepartures().get(trainNumber).setDelay(delay, "later");
  }
  
  /**
   * Mutator method updates the track of a train.
   * <p>
   *   The method is responsible for validating the parameter, and for removing the delay of a
   *   train.
   *   </p>
   * <p>
   *   Takes the parameter:
   *   <ul>
   *     <li>track: int</li>
   *     <li>trainNumber: int</li>
   *     </ul>
   *
   *
   * @param track -> the train's new track.
   * @param trainNumber -> the train number of the train whose track is being updated.
   */
  public void updateTrack(int track, int trainNumber) {
    validityManager.validateTrainNumberParameter(trainNumber, "later");
    validityManager.validateTrackParameter(track, getTrainDepartures().get(trainNumber)
            .getDepartureTime(), getTrainDepartures().get(trainNumber).getDelayedDepartureTime());
    getTrainDepartures().get(trainNumber).setTrack(track);
  }
  
  /**
   * Mutator method removes the track of a train.
   * <p>
   *   The method is responsible for validating the parameter, and for removing the track of a
   *   train.
   *   </p>
   * <p>
   *   Takes the parameter:
   *   <ul>
   *     <li>trainNumber - int</li>
   *     </ul>
   *
   *
   * @param trainNumber int representing the train number.
   */
  public void removeTrack(int trainNumber) {
    validityManager.validateTrainNumberParameter(trainNumber, "later");
    
    getTrainDepartures().get(trainNumber).removeTrack();
  }
  
  /**
   * Accessor (getter) method returns a String representation of the HashMap.
   *
   * @return String representation of the HashMap.
   */
  public String toString() {
    sortTrainDeparturesByDepartureTime();
    String header = String.format("| %-12s | %-18s | %-10s | %-15s | %-20s | %-10s | %-22s |",
        "Train", "Destination", "Line",
        "Track ", "Departure Time", "Delay", "Delayed Departure Time")
        + "\n-----------------------------------------------------------------------"
        + "-------------------------------------------------------------------";
    
    return header + "\n" + getTrainDepartures().values()
        .stream()
        .map(TrainDeparture::toSingleLineString)
        .collect(Collectors.joining("\n"));
  }
}